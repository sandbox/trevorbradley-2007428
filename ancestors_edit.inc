<?php

function ancestors_edit_form($form, &$form_state) {
  $form = array();
  
  $form['sources'] = array(
    '#title' => t('Sources'),
    '#type' => 'fieldset',
  );
  
  for ($i = 0; $i<2; $i++) {
    $form['sources'][$i] = array(
      '#title' => t('Source'),
      '#type' => 'fieldset',
      'source' => array(
        '#type' => 'textfield',
        '#title' => 'Source',
        '#default_value' => '',
        '#size' => 20,
        '#maxlength' => 255,    
      ),
      'source_reference' => array(
        '#type' => 'textarea',
        '#title' => t('Source Reference'),
        '#default_value' => '',
        '#cols' => 40,
        '#rows' => 3,
      ),
      'source_notes' => array(
        '#type' => 'textarea',
        '#title' => t('Source Notes'),
        '#default_value' => '',
        '#cols' => 40,
        '#rows' => 3,
      ),    
      'source_files' => array(
        '#title' => t('Source Files'),
        '#type' => 'fieldset',      
      ),
    );
    for ($j = 0; $j < 2; $j++) {
      $form['sources'][$i]['source_files'][$j] = array(
        '#name' => "source_files_".$i."_"
        .$j,
        '#type' => 'file',
        '#title' => t('Source File'),
        '#size' => 22,
      );
    }
  }
  
  $form['person'] = array(
    '#title' => t('Person'),
    '#type' => 'fieldset',
  );  

  $form['person']['name'] = array(
    '#title' => t('Name'),
    '#type' => 'fieldset',
  );

  for ($i = 0; $i<2; $i++) {
    /*
    $form['person']['name'][$i] = array(
      '#title' => t('Name'),
      '#type' => 'fieldset',   
    );
    */
    $form['person']['name'][$i]['name'] = array(
      '#type' => 'textfield',
      '#title' => 'Name',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,    
    );
  }
  
  $form['person']['parents'] = array(
    '#title' => t('Parents'),
    '#type' => 'fieldset',
    'father' => array(
      '#type' => 'textfield',
      '#title' => 'Father',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
    'mother' => array(
      '#type' => 'textfield',
      '#title' => 'Mother',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
  );

  $form['person']['born'] = array(
    '#title' => t('Born'),
    '#type' => 'fieldset',
    'date' => array(
      '#type' => 'textfield',
      '#title' => 'Date',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
    'location' => array(
      '#type' => 'textfield',
      '#title' => 'Location',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
  );

  for ($i=0; $i<2; $i++) {
    $form['person']['relationship'][$i] = array(
      '#title' => t('Relationship'),
      '#type' => 'fieldset',
      'relationship_with' => array(
        '#type' => 'textfield',
        '#weight' => 0,
        //'#title' => 'relationship_with',
        '#default_value' => '',
        '#size' => 20,
        '#maxlength' => 255,
      ),
      'married' => array(
        '#title' => t('Married'),
        '#type' => 'fieldset',
        '#weight' => 1,
        'date' => array(
          '#type' => 'textfield',
          '#title' => 'Date',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),
        'location' => array(
          '#type' => 'textfield',
          '#title' => 'Location',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),        
      ),
      'child' => array(      
        '#weight' => 2,
      ),
      'divorced' => array(
        '#title' => t('Divorced'),
        '#type' => 'fieldset',
        '#weight' => 999,
        'date' => array(
          '#type' => 'textfield',
          '#title' => 'Date',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),
        'location' => array(
          '#type' => 'textfield',
          '#title' => 'Location',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),        
      ),
    );
    for ($j=0;$j<3; $j++) {
      $form['person']['relationship'][$i]['child'][$j] = array(
        '#title' => t('Child'),
        '#type' => 'fieldset',
        '#weight' => $j,
        'name' => array(
          '#type' => 'textfield',
          '#title' => 'Name',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),
        'birth_date' => array(
          '#type' => 'textfield',
          '#title' => 'Birth Date',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),
        'birth_location' => array(
          '#type' => 'textfield',
          '#title' => 'Birth Location',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),        
        'death_date' => array(
          '#type' => 'textfield',
          '#title' => 'Death Date',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),
        'death_location' => array(
          '#type' => 'textfield',
          '#title' => 'Death Location',
          '#default_value' => '',
          '#size' => 20,
          '#maxlength' => 255,
        ),        
      );
    }
  }
  
  $form['person']['died'] = array(
    '#title' => t('Died'),
    '#type' => 'fieldset',
    'date' => array(
      '#type' => 'textfield',
      '#title' => 'Date',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
    'location' => array(
      '#type' => 'textfield',
      '#title' => 'Location',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
  );
    $form['person']['buried'] = array(
    '#title' => t('Buried'),
    '#type' => 'fieldset',
    'date' => array(
      '#type' => 'textfield',
      '#title' => 'Date',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
    'location' => array(
      '#type' => 'textfield',
      '#title' => 'Location',
      '#default_value' => '',
      '#size' => 20,
      '#maxlength' => 255,
    ),
  );

  dpm($form);

  return $form;
}

function theme_ancestors_edit_form($variables) {
  $form = $variables['form'];

  $output = drupal_render_children($form);

  return $output;
}


/*  
function ancestors_edit_form($form, &$form_state) {
  // Identify that the elements in 'example_items' are a collection, to
  // prevent Form API from flattening the array when submitted.

  $form['checkboxes_fieldset'] = array(
    '#title' => t("Generated Checkboxes"),
    // The prefix/suffix provide the div that we're replacing, named by
    // #ajax['wrapper'] above.
    '#prefix' => '<div id="checkboxes-div">',
    '#suffix' => '</div>',
    '#type' => 'fieldset',
    '#description' => t('This is where we get automatically generated checkboxes'),
  );  
  

  $form['example_items']['#tree'] = TRUE;

  // Fetch the example data from the database, ordered by weight ascending.
  //
  // This query excludes the last two tabledrag_example database rows, as
  // they are intended only for the 'parent/child' tabledrag examples.
  $result = db_query('SELECT id, name, description, weight FROM {tabledrag_example} WHERE id < 8 ORDER BY weight ASC');

  // Iterate through each database result.
  foreach ($result as $item) {

    // Create a form entry for this item.
    //
    // Each entry will be an array using the the unique id for that item as
    // the array key, and an array of table row data as the value.
    $form['example_items'][$item->id] = array(

      // We'll use a form element of type '#markup' to display the item name.
      'name' => array(
        '#markup' => check_plain($item->name),
      ),

      // We'll use a form element of type '#textfield' to display the item
      // description, which will allow the value to be changed via the form.
      // We limit the input to 255 characters, which is the limit we set on
      // the database field.
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => check_plain($item->description),
        '#size' => 20,
        '#maxlength' => 255,
      ),

      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $item->weight,
        '#delta' => 10,
        '#title-display' => 'invisible',
      ),
    );
  }

  $form['example_items2']['#tree'] = TRUE;

  for ($i = 1 ; $i<=3; $i++) {
    $form['example_items2'][$i] = array(
      'source' => array(
        '#type' => 'textfield',
        '#title' => 'Source',
        '#default_value' => "Source $i",
        '#size' => 60,
        '#maxlength' => 255,
      ),
      'source_reference' => array(
        '#type' => 'textarea',
        '#title' => 'Source Reference',
        '#default_value' => "Source Reference $i",
        '#cols' => 60,
        '#rows' => 3,
      ),
      'source_notes' => array(
        '#type' => 'textarea',
        '#title' => 'Source Notes',
        '#default_value' => "Source Notes $i",
        '#cols' => 60,
        '#rows' => 3,
      ),
      'source_files' => array(
        '#tree' => TRUE,
      ),
      // The 'weight' field will be manipulated as we move the items around in
      // the table using the tabledrag activity.  We use the 'weight' element
      // defined in Drupal's Form API.
      'weight' => array(
        '#type' => 'weight',
        '#title' => t('Weight'),
        '#default_value' => $i,
        '#delta' => 10,
        '#title-display' => 'invisible',
        //'#attributes' => array(
        //  'class' => 'example-item2-weight',
        //),
      ),
    );
    for ($j = 1 ; $j<=3; $j++) {
      $form['example_items2'][$i]['source_files'][$j] = array(
        'source_file' => array(
          '#type' => 'textfield',
          '#title' => 'Source File',
          '#default_value' => "Source File $i $j",
          '#size' => 60,
          '#maxlength' => 255,
        ),
      );
    }
  }

  // Now we add our submit button, for submitting the form results.
  //
  // The 'actions' wrapper used here isn't strictly necessary for tabledrag,
  // but is included as a Form API recommended practice.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

function theme_ancestors_edit_form($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();
  $output = '';

  // Iterate over each element in our $form['example_items'] array.
  foreach (element_children($form['example_items']) as $id) {

    // Before we add our 'weight' column to the row, we need to give the
    // element a custom class so that it can be identified in the
    // drupal_add_tabledrag call.
    //
    // This could also have been done during the form declaration by adding
    // '#attributes' => array('class' => 'example-item-weight'),
    // directy to the 'weight' element in tabledrag_example_simple_form().
    $form['example_items'][$id]['weight']['#attributes']['class'] = array('example-item-weight');

    // We are now ready to add each element of our $form data to the $rows
    // array, so that they end up as individual table cells when rendered
    // in the final table.  We run each element through the drupal_render()
    // function to generate the final html markup for that element.
    $rows[] = array(
      'data' => array(
        drupal_render($form['example_items'][$id]['name']) .
        drupal_render($form['example_items'][$id]['description']),
        drupal_render($form['example_items'][$id]['weight'])
      ),
      // To support the tabledrag behaviour, we need to assign each row of the
      // table a class attribute of 'draggable'. This will add the 'draggable'
      // class to the <tr> element for that row when the final table is
      // rendered.
      'class' => array('draggable'),
    );
  }

  // We now define the table header values.  Ensure that the 'header' count
  // matches the final column count for your table.
  //$header = array(t('Name'), t('Description'), t('Weight'));
  $header = array(t('Fields'),t('Weight'));

  // We also need to pass the drupal_add_tabledrag() function an id which will
  // be used to identify the <table> element containing our tabledrag form.
  // Because an element's 'id' should be unique on a page, make sure the value
  // you select is NOT the same as the form ID used in your form declaration.
  $table_id = 'example-items-table';

  // We can render our tabledrag table for output.
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements (such as our submit button).

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'example-item-weight');

  $header = array(t('Sources'));
  $table_id = 'example-items2-table';
  $rows = array();
  foreach (element_children($form['example_items2']) as $id) {

    $header2 = array(t('Files'));
    $table_id2 = "example-items-table-file-$id";
    $rows2 = array();
    foreach (element_children($form['example_items2'][$id]['source_files']) as $id2) {
      $form['example_items'][$id]['source_files'][$id2]['weight']['#attributes']['class'] = array("example-item-file-$id2-weight");

      $rows2[] = array(
        'data' => array(
          drupal_render($form['example_items2'][$id]['source_files'][$id2]['source_file']) 
        ),
        'class' => array('draggable'),
      );
    }
    $files_output = theme('table', array(
      'header' => $header2,
      'rows' => $rows2,
      'attributes' => array('id' => $table_id2),
    ));

    drupal_add_tabledrag($table_id2, 'order', 'sibling', "example-item-file-$id2-weight");
  
    $form['example_items'][$id]['weight']['#attributes']['class'] = array('example-item2-weight');

    $rows[] = array(
      'data' => array(
        drupal_render($form['example_items2'][$id]['source']) .
        drupal_render($form['example_items2'][$id]['source_reference']) .
        drupal_render($form['example_items2'][$id]['source_notes']) .
        $files_output
      ),
      'class' => array('draggable'),
    );
  }
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  drupal_add_tabledrag($table_id, 'order', 'sibling', 'example-item2-weight');

  $output .= drupal_render_children($form);

  return $output;
}
*/
?>

